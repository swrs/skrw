require 'test_helper'

module Skrw
  class UserTest < ActiveSupport::TestCase
    setup do
      @user = skrw_users(:user)
      @admin = skrw_users(:admin)
      @new_user = Skrw::User.new(password: "Abcd1234", password_confirmation: "Abcd1234")
    end

    test "has user role" do
      assert @user.role == :user
    end

    test "has admin role" do
      assert @admin.role == :admin
    end

    test "new user is valid" do
      assert @new_user.valid?
    end

    test "new user is invalid without password" do
      @new_user.password = nil
      assert_not @new_user.valid?
    end

    test "new user is invalid with wrong password confirmation" do
      @new_user.password_confirmation = "#{@new_user.password}abc"
      assert_not @new_user.valid?
    end

    test "is invalid with password length < 8" do
      password = "Abc123"
      @new_user.password = password 
      @new_user.password_confirmation = password 
      assert_not @new_user.valid?
    end

    test "is invalid without lowercase in password" do
      password = "ABCD1234"
      @new_user.password = password 
      @new_user.password_confirmation = password 
      assert_not @new_user.valid?
    end

    test "is invalid without uppercase in password" do
      password = "abcd1234"
      @new_user.password = password 
      @new_user.password_confirmation = password 
      assert_not @new_user.valid?
    end

    test "is invalid without digit in password" do
      password = "ABCDEFGH"
      @new_user.password = password 
      @new_user.password_confirmation = password 
      assert_not @new_user.valid?
    end
  end
end
