require "test_helper"

class FormTest < ActionDispatch::IntegrationTest
  include Sorcery::TestHelpers::Rails::Integration
  include Sorcery::TestHelpers::Rails::Controller

  setup do
    @admin = skrw_users(:admin)
    post skrw.create_session_path, params: { email: @admin.email, password: "password" }
  end

  test "shows title field correctly" do
    get "/posts/new"
    assert_select "label[for='post_title']", "Title *"
    assert_select "input[type='text']"
  end

  test "shows field-specific error messages" do
    post "/posts", params: { post: { title: nil} }
    assert_select ".skrw-error"
  end

  test "shows for logged in admin" do
    post skrw.create_session_path, params: { email: @admin.email, password: "password" }
    get "/"
    # page contains dashboard
    assert_select "div.skrw-dashboard"
  end
end
