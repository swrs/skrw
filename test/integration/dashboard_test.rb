require "test_helper"
require "skrw/test_helpers/integration_test/user"

class DashboardTest < ActionDispatch::IntegrationTest
  include Sorcery::TestHelpers::Rails::Integration
  include Sorcery::TestHelpers::Rails::Controller
  include Skrw::TestHelpers::IntegrationTest::User

  setup do
    @admin = skrw_users(:admin)
  end

  test "shows for logged in admin" do
    post skrw.create_session_path, params: { email: @admin.email, password: "password" }
    get "/"
    # page contains dashboard
    assert_select "div.skrw-dashboard"
  end

  test "does not show without logged in admin" do
    post skrw.destroy_session_path
    get "/" 
    assert_select "div.skrw-dashboard", false
  end

  test "does not show for logged in user" do
    @user = skrw_users(:user)
    post skrw.create_session_path, params: { email: @user.email, password: "password" }
    get "/"
    assert_select "div.skrw-dashboard", false
  end

  test "has contents set by content_for block" do
    post skrw.create_session_path, params: { email: @admin.email, password: "password" }
    get "/"
    assert_select "a", "Posts" 
    assert_select "a", "New Post" 
  end

  test "shows flash message for session without logged in user" do
    post skrw.create_session_path, params: { email: @admin.email, password: "wrong" }
    assert_select "div.skrw-flash"
  end

  test "shows breadcrumbs" do
    login(@admin)
    get main_app.new_post_path
    assert_select ".skrw-dashboard__breadcrumbs" do
      assert_select "a[href*='dashboard']"
      assert_select "a[href*='posts']"
      assert_select "a[href*='posts/new']"
    end
  end
end
