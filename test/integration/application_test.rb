require "test_helper"
require "skrw/test_helpers/integration_test/user"

class ApplicationTest < ActionDispatch::IntegrationTest
  include Skrw::TestHelpers::IntegrationTest::User

  setup do
    @admin = skrw_users(:admin)
    login @admin, "password"
  end

  test "redirects to referrer" do
    referrer = root_path
    get new_post_path(referrer: referrer)
    post posts_path, params: { post: { title: "abc" } }
    assert_redirected_to referrer
    assert_nil session[:referrer]
  end

  test "redirects to referrer with fallback" do
    post posts_path, params: { post: { title: "abc" } }
    assert_redirected_to post_path(id: Post.last.id)
  end
end
