require "test_helper"

class Skrw::FormatHelperTest < ActionView::TestCase
  include Skrw::FormatHelper

  test "markdown formats html with default settings" do
    text = "abc123"
    assert_match /<p>.+<\/p>/, skrw_markdown(text)
  end

  test "markdown returns nil for empty string" do
    assert skrw_markdown(nil).nil?
  end
end
