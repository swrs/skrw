Rails.application.routes.draw do
  mount Skrw::Engine => "/skrw"

  get "skrw/dashboard", to: "admin#dashboard", as: :admin_dashboard

  resources :posts

  root to: "posts#index"
end
