module Skrw
  class DashboardPolicy < Struct.new(:user, :dashboard)
    def show?
      user.present? && user.role == :admin
    end
  end
end
