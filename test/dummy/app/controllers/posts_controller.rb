class PostsController < ApplicationController
  before_action :set_post, only: [:show, :edit, :update, :destroy]
  before_action except: [:index, :show] do
    require_login_with_role([:admin])
  end
  before_action :set_referrer

  breadcrumb "Dashboard", :admin_dashboard_path
  breadcrumb "Posts", :posts_path, only: [:new, :edit]

  # GET /posts
  def index
    @posts = Post.all
  end

  # GET /posts/1
  def show
  end

  # GET /posts/new
  def new
    breadcrumb "New Post", new_post_path
    @post = Post.new
  end

  # GET /posts/1/edit
  def edit
    breadcrumb "Edit #{@post.title}", edit_post_path(@post)
  end

  # POST /posts
  def create
    @post = Post.new(post_params)

    if @post.save
      redirect_to_referrer post_path(@post)
    else
      render :new
    end
  end

  # PATCH/PUT /posts/1
  def update
    if @post.update(post_params)
      redirect_to @post, notice: 'Post was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /posts/1
  def destroy
    @post.destroy
    redirect_to posts_url, notice: 'Post was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Post.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def post_params
      params.require(:post).permit(:title)
    end
end
