require "test_helper"
require "skrw/test_helpers/integration_test/user"

module Skrw
  class SessionsControllerTest < ActionDispatch::IntegrationTest
    include Engine.routes.url_helpers
    include Skrw::TestHelpers::IntegrationTest::User

    setup do
      @admin = skrw_users(:admin)
    end

    test "get login is success" do
      get skrw.new_session_path
      assert_response :success
    end

    test "get login is redirected for logged in users" do
      post skrw.create_session_path, params: { email: @admin.email, password: "password" }
      get skrw.new_session_path
      assert_response :redirect
    end

    test "login with correct credentials is success" do  
      post skrw.create_session_path, params: { email: @admin.email, password: "password" }
      assert_response :redirect
    end

    test "login with wrong credentials is unauthorized" do  
      post skrw.create_session_path, params: { email: @admin.email, password: "wrong" }
      assert_response 401
    end

    test "logout is success" do
      post skrw.create_session_path, params: { email: @admin.email, password: "password" }
      post skrw.destroy_session_path
      assert_response :redirect
    end

    test "does not save user ip address" do
      login(@admin)
      assert_nil @admin.reload.last_login_from_ip_address, "user ip address is nil"
    end
  end
end
