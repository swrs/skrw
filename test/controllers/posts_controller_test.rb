require "test_helper"

class PostsControllerTest < ActionDispatch::IntegrationTest

  setup do
    @user = skrw_users(:user)
    @admin = skrw_users(:admin)
  end

  test "new post without login redirects" do
    get new_post_path
    assert_redirected_to skrw.new_session_path
    assert_not_empty flash[:error]
  end

  test "new post as user redirects" do
    post skrw.create_session_path, params: { email: @user.email, password: "password" }
    get "/posts/new"
    assert_redirected_to skrw.new_session_path
    assert_not_empty flash[:error]
  end

  test "new post as admin is success" do
    post skrw.create_session_path, params: { email: @admin.email, password: "password" }
    get "/posts/new" 
    assert_response :success
  end
end
