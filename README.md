<img src="lib/skrw.png" width="375">

# Skrw

*Skrw* is a lightweight engine for authentication and content management. To put it in a nutshell, it provides some help around recurring tasks like installing an authentication gem and setting up administration users, or displaying a toolbar with flash messages. *Skrw* works with [*skrw-js*](https://gitlab.com/swrs/skrw-js) which provides all necessary styles and javascript.

## Requirements

- [Rails](https://github.com/rails/rails) >= 6.0.3.1

## Installation

Currently *Skrw* is not on RubyGems and installed via git:

Add *Skrw* and *Sorcery* to your gems:

```ruby
gem "skrw", git: "https://gitlab.com/swrs/skrw.git", branch: "2.0.0"
```

to install [*skrw-js*](https://gitlab.com/swrs/skrw-js) (necessary) see [*skrw-js*](https://gitlab.com/swrs/skrw-js) docs.

Mount *Skrw* in host's `routes.rb`:

```ruby
Rails.application.routes.draw do
  # …
  mount Skrw::Engine => "/skrw"
  # …
end
```

Install the necessary migrations for the `User` model and migrate the database:

```bash
rails skrw:install:migrations
rails db:migrate
```

Create user models programatically:

```ruby
user = Skrw::User.create(email: "somebody@hollywood.com", password: "palmtree", password_confirmation: "palmtree")
admin = Skrw::User.create(email: "someother@bollywood.com", password: "coco", password_confirmation: "coco", role: "admin")
```

## Usage

- [Dashboard](#dashboard)
- [User Model](#user-model)
- [Authentication](#authentication)
- [Policies](#policies)
- [Redirection](#redirection)
- [Testing](#testing)

<a name="dashboard"></a>
### Dashboard

The dashboard is thought of as a main access to any CMS functionalities, nn the end it's just a plain toolbar. The dashboard is visible to any logged in user (optionally restricted by `role`), it can contain any site- or page-specific code and displays *Skrw*'s flash messages.

To display the dashboard use the `skrw_dashboard` helper.

```ruby
# in app/views/layouts/application.html.erb

# visible to any logged in user
<%= skrw_dashboard %>

# visible to any logged in user with the role :admin or :editor
<%= skrw_dashboard [:admin, :editor] %>
```

#### Dashboard contents

The contents of the dashboard are set by `content_for :skrw_dashboard`. Setting `content_for :skrw_dashboard` can be set multiple times and be especially helpful to change or prepend any code to the dashboard contents on a page-sepcific basis.

```ruby
# set content with content_for
<% content_for :skrw_dashboard do %>
  <%= link_to "Posts", posts_path %>
  <%= link_to "New Post", new_post_path %>
<% end %>
```

```ruby
# by default content_for appends to previous definitions, thus the content_for
# in edit.hrml.erb will append to the previous content_for in application.html.erb

# app/views/layoutes/application.html.erb
<% content_for :skrw_dashboard do %>
  <%= link_to "Posts", posts_path %>
  <%= link_to "New Post", new_post_path %>
<% end %>

# app/views/posts/edit.html.erb
<% content_for :skrw_dashboard do %>
  <%= link_to "Delete Post", post_path(@post), method: :delete %>
<% end %>
```

```ruby
# to override any previous content_for definitions use flush: true
<% content_for :skrw_dashboard, flush: true do %>
  <%= link_to "Posts", posts_path %>
  <%= link_to "New Post", new_post_path %>
<% end %>
```

#### Dashboard breadcrumbs

The dashboard features a breadcrumb navigation using the [*Loaf*](https://github.com/piotrmurach/loaf) gem. Consult Loaf's docs for detailed instructions, a basic could be:

```ruby
# config/routes.rb
get "admin/dashboard", to: "admin#dashboard"

namespace :admin do
  resources :posts
end

# app/controllers/admin_controller.rb
class AdminController < ApplicationController
  breadcrumb "Dashboard", :admin_dashboard_path
end

# app/controllers/admin/posts_controlle.rb
class Admin::PostsController < AdminController
  breadcrumb "Posts", :admin_posts_path, only: [:index, :new, :edit]
  
  def new
    breadcrumb "New Post", :admin_posts_path
  end
end
```

Visiting `admin/posts/new` would output the breadcrumb links to `/admin/dashboard`, `/admin/posts`, and `/admin/posts/new` in *Skrw*'s dashboard.

```erb
<ul class="skrw-breadcrumbs">
  <li><a href="/admin/dashboard">Dashboard</a></li>
  <li><a href="/admin/posts">Posts</a></li>
  <li><a href="/admin/posts/new">New Post</a></a></li>
</ul>
```

<a name="user-model"></a>
### User Model

The `User` must have an email address (unique) and a password (min. 8 characters therof one capital, at least 1 digit). The user's *role* can be set to any string with the `role` attribute (default: `"user"`) and is useful in conjunction with the *policies* provided by the [Pundit](https://github.com/varvet/pundit) gem. To create a user:

```ruby
user = Skrw::User.create(email: "somebody@hollywood.com", password: "palmtree", password_confirmation: "palmtree")
admin = Skrw::User.create(email: "someother@bollywood.com", password: "coco", password_confirmation: "coco", role: "admin")
```

<a name="authentication"></a>
### Authentication

*Skrw* uses the [*Sorcery*](https://github.com/Sorcery/sorcery) gem to authenticate users, thus it has all of *Sorcery*'s tools at hand and provides some additional functionality.

The most important authentication mechanisms are:

```ruby
require_login # in controllers to require a login before continuing
current_user # returns the currently logged in user (available in views)
```

Additionally to *Sorcery*'s functionalities, *Skrw* provides:

```ruby
require_login_with_role(roles = []) # in controllers to require a login with specific roles before continuing

# example usage:
before_action only: [:new, :create] do
  require_login_with_role([:admin])
end
```

```ruby
# view helper to check if a user is logged in or is accessing a controller
# method using the SessionController
user_session?(roles = []) 

# view helper to check if a user is logged in AND has one of the roles
# defined in the roles array
logged_in_with_role?(roles = [])
```

<a name="policies"></a>
### Policies

The *policies* are stored in `app/policies/skrw` and are used to authorize the access of users, they are basically just a regular implementation of the [Pundit](https://github.com/varvet/pundit) gem. To know how to use *policies* read the docs of [Pundit](https://github.com/varvet/pundit).

<a name="redirection"></a>
### Redirection

In some cases you might have to redirect the user to a specific page across multiple requests. This can be done via session using `set_referrer` and `redirect_to_referrer` in your controllers. 

In the following example the user clicks on the *New post* link, followed by the creation of a new post, finally being redirected to `my_custom_path` by the `create` action. The link of *New post* has a `referrer` parameter which stores `my_custom_path` as a referrer in the user's session thanks to the `set_referrer` method called before `new`. The `create` action uses `redirect_to_referrer` which checks for the existence of a value for `:referrer` in the session and redirects to it. If `:referrer` would be nil, the redirection would fall back to `posts_path`.

```erb
<%= link_to "New post", new_post_path(referrer: my_custom_path) %>
```

```ruby
class PostsController < ApplicationController
  before_action :set_referrer

  def new
    @post = Post.new
  end

  def create
    @post = Post.new post_params

    if @post.save
      redirect_to_referrer posts_path
    end
  end
end
```

<a name="testing"></a>
### Testing

*Skrw* comes with test helpers for integration tests to provide some handy methods for logging in and out (once again, based on *Sorcery*). See the available test helpers at `lib/skrw/test_helpers/`.

To use the login helper for integration tests:

```ruby
require "test_helper"
require "skrw/test_helpers/integration_test/user"

class PostsControllerTest < ActionDispatch::IntegrationTest
  include Skrw::TestHelpers::IntegrationTest::User
  
  test "creates post" do
    @user = skrw_users(:user)
    login(@user)
    # or provide a password if user password != "password"
    # login(@user, "apples")
    # test your things
  end
end
```

## Credits

The whole *Skrw* engine is really just a wrapper around some functionalities and makes heavy use of already existing gems. Therefore all the credits go to the gems (and their creators) it relies on:

- [Rails](https://github.com/rails/rails)
- [Sorcery](https://github.com/Sorcery/sorcery)
- [Pundit](https://github.com/varvet/pundit)
- [Icon](lib/skrw.png) by Lucas Lejeune
