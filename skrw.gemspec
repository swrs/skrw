$:.push File.expand_path("lib", __dir__)

# Maintain your gem's version:
require "skrw/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |spec|
  spec.name        = "skrw"
  spec.version     = Skrw::VERSION
  spec.authors     = ["swrs"]
  spec.email       = ["mail@pbernhard.com"]
  spec.homepage    = "https://gitlab.com/swrs/skrw"
  spec.summary     = "Lightweight authentication and CMS engine for Rails."
  spec.description = spec.summary
  spec.license     = "MIT"

  spec.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  spec.add_dependency "rails", "~> 7.0"
  spec.add_dependency "pundit", "~> 2.1.0"
  spec.add_dependency "sorcery", "~> 0.16.0", ">= 0.16.0"
  spec.add_dependency "redcarpet", "~> 3.5", ">= 3.5.0"
  spec.add_dependency "loaf", "~> 0.9", ">= 0.9.0"

  spec.add_development_dependency "sqlite3"
end
