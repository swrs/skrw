class AddRememberMeToSkrwUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :skrw_users, :remember_me_token, :string, default: nil
    add_column :skrw_users, :remember_me_token_expires_at, :datetime, default: nil

    add_index :skrw_users, :remember_me_token
  end
end
