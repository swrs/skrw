class AddRoleToSkrwUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :skrw_users, :role, :string, default: "user"
    add_index :skrw_users, :role
  end
end
