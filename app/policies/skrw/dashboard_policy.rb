module Skrw
  class DashboardPolicy < Struct.new(:user, :dashboard) 

    # show dashboard?
    # user = current_user
    def show?
      !user.nil?
    end
  end
end
