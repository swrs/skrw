require_dependency "skrw/application_controller"

module Skrw
  class SessionsController < Skrw::ApplicationController

    def new
      if current_user
        redirect_to main_app.root_path and return
      end
    end

    def create
      @user = login(params[:email], params[:password], params[:remember])

      if @user
        redirect_back_or_to(main_app.root_path)
      else
        flash[:skrw] = t("sessions.login_failed")
        render :new, status: 401
      end
    end

    def destroy
      if current_user
        logout
        redirect_to main_app.root_path
      end
    end
  end
end
