module Skrw::ApplicationControllerConcern

  # what to do when "not authenticated"?
  def not_authenticated
    case request.format.to_sym
    when :json
      render body: nil, status: :unauthorized
    else
      flash[:error] = t("sessions.not_authenticated") if flash[:error].blank?
      redirect_to skrw.new_session_path
    end
  end

  # is logged in with role?
  def logged_in_with_role?(roles = [])
    logged_in? && roles.include?(current_user.role) ? true : false
  end

  # require_login for roles
  def require_login_with_role(roles = [])
    if roles.blank?
      require_login
    else
      return if logged_in_with_role?(roles)

      logout if logged_in?
      require_login
    end
  end

  # set referrer
  def set_referrer
    session[:referrer] = params[:referrer] if params[:referrer].present?
  end

  # redirect with referrer
  def redirect_to_referrer(fallback, **options)
    fallback = session[:referrer] || fallback
    session.delete :referrer
    redirect_to fallback, options
  end
end
