module Skrw
  class User < ApplicationRecord
    authenticates_with_sorcery!

    # regexp for password validation
    PASSWORD_FORMAT = /
      (?=.{8,}) # at least 8 chars long
      (?=.*\d) # at least one digit
      (?=.*[a-z]) # at least one digit
      (?=.*[A-Z]) # at least one digit
    /x
 
    # password validation
    validates :password, format: { with: PASSWORD_FORMAT, 
      message: "needs to have at least: 8 characters, 1 uppercase character, 1 lowercase character, 1 digit" },
      if: -> { new_record? || changes[:crypted_password] }
    validates :password, confirmation: true, if: -> { new_record? || changes[:crypted_password] }

    # return role as symbol
    def role
      self[:role].to_sym
    end
  end
end
