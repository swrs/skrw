class Skrw::FormBuilder < ActionView::Helpers::FormBuilder
  
  # field wrapper with label and field-specific error messages
  def field(attr, label = nil, **options, &block)
    errors = @object.errors[attr] unless @object.nil?
    options[:class] = "#{options[:class]} skrw-form-field".strip

    # build field output
    @template.content_tag(:div, options) do
      @template.concat self.label(attr, label) unless label.nil?
      @template.concat @template.capture(&block)
      @template.concat self.errors(errors) unless errors.blank?
    end
  end

  # label with asteriks for mandatory attributes
  def label(attr, text = nil, **options, &block)

    # check for presence validators on attr
    unless @object.nil? || text.nil?
      presence_validators = @object.class.validators.select do |v|
        v.is_a?(ActiveRecord::Validations::PresenceValidator) && v.attributes.include?(attr)
      end

      text += " *" if presence_validators.any?
    end

    # return modified label
    super(attr, text, options, &block)
  end

  # errors list
  def errors(errors = [])
    if errors.any?
      @template.content_tag(:ul) do
        errors.each do |error|
          @template.concat @template.content_tag(:li, error, class: "skrw-error")
        end
      end
    end
  end
end
