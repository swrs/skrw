module Skrw
  module FormatHelper
    
    # formatted date
    def skrw_formatted_date(date, time = true)
      date.strftime("%d.%m.%Y#{time ? ' %H:%M:%S' : ''}")
    end

    def skrw_timestamp(record, created_at = false)
      capture do
        concat "Created at: #{skrw_formatted_date(record.created_at)}<br>".html_safe if created_at
        concat "Updated at: #{skrw_formatted_date(record.updated_at)}".html_safe
      end
    end

    # markdown helper using Redcarpet parser
    def skrw_markdown(text, extensions: {}, renderer: Redcarpet::Render::HTML, render_options: {})
      extensions[:autolink] = extensions.key?(:autolink) 

      # set defaults for extensions
      defaults = {
        extensions: {
          autolink: true,
          underline: true
        },
        render_options: {}
      }

      extensions = defaults[:extensions].merge(extensions)
      render_options = defaults[:render_options].merge(render_options)

      markdown = Redcarpet::Markdown.new(renderer.new(render_options), extensions) 
      text.blank? ? nil : markdown.render(text)
    end
  end
end
