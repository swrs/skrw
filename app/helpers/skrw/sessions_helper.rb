module Skrw
  module SessionsHelper
    
    # dashboard
    # available to all roles in roles array
    # contents are set by content_for(:skrw_dashboard)
    # accepts block which prepends to content_for(:skrw_dashboard)
    # display flash messages
    def skrw_dashboard(**options, &block)
      capture do
        if user_session?
          concat render partial: "skrw/sessions/flash"
        end

        if policy([:skrw, :dashboard]).show?
          concat render("skrw/sessions/dashboard", options: options)
        end
      end
    end

    # is user logged in or request to skrw/sessions controller
    def user_session?(roles = [])
      logged_in_with_role?(roles) || params[:controller] == "skrw/sessions" ? true : false
    end

    # is logged in and part of roles array
    def logged_in_with_role?(roles = [])
      logged_in? && (roles.blank? || roles.include?(current_user.role)) ? true : false
    end
  end
end
