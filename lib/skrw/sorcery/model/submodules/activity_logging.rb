require "sorcery/model/submodules/activity_logging"

module Sorcery
  module Model
    module Submodules
      module ActivityLogging
        module InstanceMethods

          # override set_last_up_address to always set nil as ip address
          def set_last_ip_address(ip_address)
            sorcery_adapter.update_attribute(sorcery_config.last_login_from_ip_address_name, nil)
          end
        end
      end
    end
  end
end
