require "sorcery"
require "pundit"
require "redcarpet"
require "loaf"

module Skrw
  class Engine < ::Rails::Engine
    isolate_namespace Skrw

    config.autoload_once_paths += %W(
      #{root}/app/controllers
      #{root}/app/helpers
    )

    # include helpers and methods
    initializer("include_helpers_and_methods") do
      ActiveSupport.on_load(:action_controller_base) do
        include Skrw::ApplicationControllerConcern
        include Pundit
        helper Skrw::SessionsHelper
        helper Skrw::FormatHelper
      end
    end
  end
end
