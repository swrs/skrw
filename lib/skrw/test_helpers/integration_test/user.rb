module Skrw
  module TestHelpers
    module IntegrationTest
      module User

        def login(user, password = "password")
          post skrw.create_session_path(email: user.email, password: password)
          follow_redirect!
        end

        def logout
          post skrw.destroy_session_path
          follow_redirect!
        end
      end
    end
  end
end
