Skrw::Engine.routes.draw do
  # login / -out with SessionsController
  get "login", to: "sessions#new", as: :new_session
  post "login", to: "sessions#create", as: :create_session
  post "logout", to: "sessions#destroy", as: :destroy_session
end
